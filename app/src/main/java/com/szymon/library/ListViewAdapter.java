package com.szymon.library;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Book> {

    public ListViewAdapter(Context context, List<Book> books) {
        super(context, R.layout.custom_booklist , books);
    }

    static class ViewHolder{
        public TextView title;
        public TextView author;
        public TextView available;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View customView = convertView;
        if (customView==null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            customView = layoutInflater.inflate(R.layout.custom_booklist, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.author = (TextView) customView.findViewById(R.id.custom_author);
            viewHolder.title = (TextView) customView.findViewById(R.id.custom_title);
            viewHolder.available = (TextView) customView.findViewById(R.id.custom_available);
            customView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) customView.getTag();
        }
        Book book = getItem(position);
        viewHolder.title.setText(book.getTitle());
        viewHolder.author.setText(book.getAuthor());
        if (book.isAvailable()){
            viewHolder.available.setText("Dostępna");
        } else {
            viewHolder.available.setText("Niedostępna");
        }
        return customView;
    }
}
