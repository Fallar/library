package com.szymon.library;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView mainListView;
    private MainDatabaseSetter mainDatabaseSetter;
    private ArrayAdapter<Book> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUiInterface();
        initData();
    }

    public void initData(){
        mainDatabaseSetter = new MainDatabaseSetter(getApplicationContext());
        mainDatabaseSetter.fillDatabaseWithBooks();
        viewAllBooks();
    }

    public void viewAllBooks(){
        setList(mainDatabaseSetter.getBookListFrom(SqlConstans.MAIN_TABLE_NAME));
    }

    public void setList(List<Book> booksToView){
        arrayAdapter = new ListViewAdapter(this,booksToView);
        assert mainListView != null;
        mainListView.setAdapter(arrayAdapter);
    }

    public void initUiInterface(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mainListView = (ListView) findViewById(R.id.books_listView);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long positionInDataBase = id + 1;
                Book book = mainDatabaseSetter.getBook(positionInDataBase);
                manageOnClickEvent(book, positionInDataBase);
            }
        });
    }

    public void manageOnClickEvent(Book book, long positionInDataBase){
        if (book.isAvailable()) {
            mainDatabaseSetter.changeStateOfBook(positionInDataBase);
            insertDataAbout(book);
            Toast.makeText(getApplicationContext(), "Wpożyczyłeś książkę: " + book.getTitle(), Toast.LENGTH_SHORT).show();
            refreshList();
        } else {
            mainDatabaseSetter.changeStateOfBook(positionInDataBase);
            deleteDataAbout(book);
            Toast.makeText(getApplicationContext(), "Oddałeś książkę: " + book.getTitle(), Toast.LENGTH_SHORT).show();
            refreshList();
        }
    }

    public void insertDataAbout(Book book){
        mainDatabaseSetter.insertEntryToUsersDatabase(book);
        mainDatabaseSetter.insertEntryToInfoDatabase(book);
    }

    public void deleteDataAbout(Book book){
        mainDatabaseSetter.deleteEntryFromTable(SqlConstans.USER_TABLE_NAME,book);
        mainDatabaseSetter.deleteEntryFromTable(SqlConstans.INFO_TABLE_NAME, book);
    }

    public void refreshList(){
        arrayAdapter.clear();
        viewAllBooks();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.all_books:
                viewAllBooks();
                mainListView.setEnabled(true);
                break;
            case R.id.available:
                setNewListView(true);
                break;
            case R.id.not_available:
                setNewListView(false);
                break;
            case R.id.my_books:
                setList(mainDatabaseSetter.getBookListFrom(SqlConstans.USER_TABLE_NAME));
                mainListView.setEnabled(false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setNewListView(boolean state){
        setList(mainDatabaseSetter.getBooksByAvailable(state));
        mainListView.setEnabled(false);
    }
}