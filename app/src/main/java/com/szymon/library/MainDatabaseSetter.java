package com.szymon.library;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.LinkedList;
import java.util.List;

public class MainDatabaseSetter extends SQLiteOpenHelper {

    public MainDatabaseSetter(Context context) {
        super(context, "library.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Główna baza
        db.execSQL(SqlConstans.MAIN_TABLE);
        db.execSQL(SqlConstans.USER_TABLE);
        //Dodatkowa baza pozostająca po stronie głównej bazy
        //Przechowuje informacje o wypożyczeniach
        db.execSQL(SqlConstans.INFO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void fillDatabaseWithBooks(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(SqlConstans.MAIN_TABLE_NAME, SqlConstans.columns, null, null, null, null, null);
        if (!cursor.moveToNext()){
            insertBook("Pan Tadeusz","Adam Mickiewicz");
            insertBook("W pustyni i w Puszczy","Henryk Sienkiewicz");
            insertBook("Chłopi","Władysław Reymont");
            insertBook("Antygona","Sofokles");
            insertBook("Sklepik z marzeniami","Stephen King");
            insertBook("Ludzie Bezdomni", "Stefan Żeromski");
            insertBook("Dżuma", "Albert Camus");
            insertBook("Mistrz i Małgorzata", "Michaił Bułchakov");
            insertBook("Wesele", "Stanisław Wyspiański");
            insertBook("Iliada", "Homer");
            Log.d(SqlConstans.DEBUG_TAG, "List created...");
        }
        cursor.close();
    }

    public void insertBook(String title,String author){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SqlConstans.TITLE_KEY,title);
        values.put(SqlConstans.AUTHOR_KEY,author);
        db.insertOrThrow(SqlConstans.MAIN_TABLE_NAME, null, values);
    }

    public Book fillBookWithData(Book book, Cursor cursor){
        book.setId(cursor.getInt(SqlConstans.ID_COLUMN));
        book.setTitle(cursor.getString(SqlConstans.TITLE_COLUMN));
        book.setAuthor(cursor.getString(SqlConstans.AUTHOR_COLUMN));
        book.setAvailable(cursor.getInt(SqlConstans.AVAILABLE_COLUMN) > 0);
        return book;
    }

    public List<Book> getBookListFrom(String tableName){
        List<Book> allbooks = new LinkedList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(tableName, SqlConstans.columns, null, null, null, null, null);
        while (cursor.moveToNext()){
            Book book = new Book();
            fillBookWithData(book, cursor);
            allbooks.add(book);
        }
        cursor.close();
        return allbooks;
    }

    public Book getBook(long choice){
        SQLiteDatabase db = getReadableDatabase();
        String[] args = {choice+""};
        Book book = new Book();
        Cursor cursor = db.query(SqlConstans.MAIN_TABLE_NAME, SqlConstans.columns, "ID=?", args, null, null, null, null);
        if (cursor.moveToNext()){
            fillBookWithData(book,cursor);
        }
        cursor.close();
        return book;
    }

    public List<Book> getBooksByAvailable(boolean available){
        List<Book> booksByAvailability = new LinkedList<>();
        String[] intAvailable = {(available ? 1:0)+""};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select ID, TITLE, AUTHOR, AVAILABLE from library where AVAILABLE=? order by ID", intAvailable);
        while (cursor.moveToNext()){
            Book book = new Book();
            fillBookWithData(book,cursor);
            booksByAvailability.add(book);
        }
        cursor.close();
        return booksByAvailability;
    }

    public void changeStateOfBook(long choice){
        String[] args = {choice+""};
        Book bookToChange = getBook(choice);
        if(bookToChange.isAvailable()){
            changeAvailability(false,args);
        }else{
            if (bookIsInUsersList(bookToChange)){
                changeAvailability(true, args);
            }
        }
    }

    public void changeAvailability(boolean newState, String[] args){
        int newValue = newState ? 1:0;
        SQLiteDatabase wdb = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SqlConstans.AVAILABLE_KEY, newValue);
        wdb.update(SqlConstans.MAIN_TABLE_NAME, values, "ID=?", args);
    }

    public boolean bookIsInUsersList(Book book) {
        boolean isInList=false;
        for(Book checkedbook : getBookListFrom(SqlConstans.USER_TABLE_NAME)){
            if (checkedbook.getTitle().equals(book.getTitle())){
                isInList=true;
            }
        }
        return isInList;
    }

    public void insertEntryToUsersDatabase(Book book){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SqlConstans.TITLE_KEY, book.getTitle());
        values.put(SqlConstans.AUTHOR_KEY, book.getAuthor());
        values.put(SqlConstans.BOOK_ID_KEY, book.getId());
        db.insertOrThrow(SqlConstans.USER_TABLE_NAME, null, values);
    }

    public void insertEntryToInfoDatabase(Book book){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SqlConstans.BOOK_ID_KEY,book.getId());
        values.put(SqlConstans.USER_ID_KEY,SqlConstans.USER_ID);
        db.insertOrThrow(SqlConstans.INFO_TABLE_NAME, null, values);
    }

    public void deleteEntryFromTable(String tableName, Book book) {
        SQLiteDatabase db = getWritableDatabase();
        String[] args = {book.getId()+""};
        db.delete(tableName, "BOOK_ID=?", args);
    }
}