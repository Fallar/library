package com.szymon.library;

public class SqlConstans {

    public static final int USER_ID = 1;
    public static final String DEBUG_TAG = "SQLITE";
    public static final String MAIN_TABLE_NAME = "library";
    public static final String USER_TABLE_NAME = "users";
    public static final String ID_KEY = "ID";
    public static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final int ID_COLUMN = 0;
    public static final String TITLE_KEY = "TITLE";
    public static final String TITLE_OPTIONS = "TEXT NOT NULL";
    public static final int TITLE_COLUMN = 1;
    public static final String AUTHOR_KEY = "AUTHOR";
    public static final String AUTHOR_OPTIONS = "TEXT NOT NULL";
    public static final int AUTHOR_COLUMN = 2;
    public static final String AVAILABLE_KEY = "AVAILABLE";
    public static final String AVAILBLE_OPTIONS = "INTEGER DEFAULT 1";
    public static final int AVAILABLE_COLUMN = 3;
    public static final String INFO_TABLE_NAME = "info";
    public static final String BOOK_ID_KEY = "BOOK_ID";
    public static final String BOOK_ID_OPTIONS = "INTEGER";
    public static final String USER_ID_KEY = "CUSTOMER_ID";
    public static final String USER_ID_OPTIONS = "INTEGER";
    public static final String[] columns = {ID_KEY,TITLE_KEY,AUTHOR_KEY,AVAILABLE_KEY};
    public static final String MAIN_TABLE = "CREATE TABLE " + MAIN_TABLE_NAME + "( "
            + ID_KEY + " " + ID_OPTIONS + ", "
            + TITLE_KEY + " " + TITLE_OPTIONS + ", "
            + AUTHOR_KEY + " " + AUTHOR_OPTIONS + ", "
            + AVAILABLE_KEY + " " + AVAILBLE_OPTIONS + ");"
            + "";
    public static final String USER_TABLE = "CREATE TABLE " + USER_TABLE_NAME + "( "
            + ID_KEY + " " + ID_OPTIONS + ", "
            + TITLE_KEY + " " + TITLE_OPTIONS + ", "
            + AUTHOR_KEY + " " + AUTHOR_OPTIONS + ", "
            + AVAILABLE_KEY + " " + AVAILBLE_OPTIONS + ", "
            + BOOK_ID_KEY + " " + BOOK_ID_OPTIONS + ");"
            + "";
    public static final String INFO_TABLE = "CREATE TABLE " + INFO_TABLE_NAME + "( "
            + ID_KEY + " " + ID_OPTIONS + ", "
            + BOOK_ID_KEY + " " + BOOK_ID_OPTIONS + ", "
            + USER_ID_KEY + " " + USER_ID_OPTIONS + ");"
            + "";
}
